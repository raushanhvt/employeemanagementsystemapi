package com.example.ems.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.ems.entity.Employee;


public interface EmployeeService {

	List<Employee> getAllEmp();
	Employee getByIdEmp(Long id);
	Employee saveEmp(Employee emp);
	Employee updateEmp(Employee emp, Long id);
	void deleteEmp(Long id);
}
