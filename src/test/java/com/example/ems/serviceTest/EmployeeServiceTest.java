package com.example.ems.serviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.ems.entity.Employee;
import com.example.ems.repository.EmployeeRepository;
import com.example.ems.service.EmployeeService;
import com.example.ems.serviceimpl.EmployeeServiceImpl;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {
	
	@Mock
	private EmployeeRepository employeeRepository;
	
	@InjectMocks
	private EmployeeServiceImpl service;
	
	@Test
	void shouldSavedEmployeeSucessfully() {
		Employee employee = new Employee("Raushan", "raushan@gmail.com", "3303830");
		when(employeeRepository.save(employee)).thenReturn(employee);
		
		Employee emp = service.saveEmp(employee);
		
		assertEquals(emp, employee);
		
	}
	
	

}
