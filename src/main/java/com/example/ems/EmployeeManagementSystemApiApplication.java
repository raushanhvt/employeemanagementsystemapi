package com.example.ems;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.ems.entity.Employee;
import com.example.ems.repository.EmployeeRepository;

@SpringBootApplication
public class EmployeeManagementSystemApiApplication  {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeManagementSystemApiApplication.class, args);
	}
	
//	@Autowired
//	private EmployeeRepository employeeRepository;
//
//	@Override
//	public void run(String... args) throws Exception {
//	
//		Employee e1 = new Employee("Raushan", "raushan@gmail.com", "1234");
//		employeeRepository.save(e1);
//		Employee e2 = new Employee("Kumar", "kumar@gmail.com", "3424");
//		employeeRepository.save(e2);
//		Employee e3 = new Employee("Rk", "rk@gmail.com", "983739");
//		employeeRepository.save(e3);
//		
//	}

}
