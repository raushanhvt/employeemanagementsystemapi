package com.example.ems.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.ems.entity.Employee;
import com.example.ems.service.EmployeeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/employees")
@Api(value = "ems", description = "Operations pertaining to employees in Employee Management System")
public class EmployeeController {

	private EmployeeService employeeService;

	public EmployeeController(EmployeeService employeeService) {
		super();
		this.employeeService = employeeService;
	}

	// Get all employees
	@GetMapping(produces = "application/json")
	@ApiOperation(value = "View a list of Employee", response = Employee.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "forbidden"), @ApiResponse(code = 404, message = "Resource Not Found") })
	public List<Employee> getAllEmp() {
		return employeeService.getAllEmp();
	}

	// Get by Id
	@GetMapping("{id}")
	@ApiOperation(value = "Get employee by Id", response = Employee.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved employee by Id"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "forbidden"), @ApiResponse(code = 404, message = "Resource Not Found") })
	public Employee getByIdEmp(@PathVariable Long id) {
		return (employeeService.getByIdEmp(id));
	}

	// Add Employee
	@PostMapping(produces = "application/json")
	@ApiOperation(value = "Add a employee", response = Employee.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Sucessfully added employee to DB"),
			@ApiResponse(code = 500, message = "Internal server error") })
	public Employee addEmployee(@RequestBody Employee emp) {
		return employeeService.saveEmp(emp);

	}

	// Update Employee
	@PutMapping("{id}")
	@ApiOperation(value = "Update an employee using Id", response = Employee.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Sucessfully updated existing employee to DB"),
			@ApiResponse(code = 500, message = "Internal server error") })
	public Employee updateEmp(@RequestBody Employee emp, @PathVariable Long id) {
		return employeeService.updateEmp(emp, id);
	}

	// Delete Employee
	@DeleteMapping("{id}")
	@ApiOperation(value = "Delete an employee using Id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully deleted employee by Id"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "forbidden"), @ApiResponse(code = 404, message = "Resource Not Found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	public void deleteEmp(@PathVariable Long id) {
		employeeService.deleteEmp(id);
	}

}
