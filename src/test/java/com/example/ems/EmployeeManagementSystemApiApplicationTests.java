package com.example.ems;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.ems.entity.Employee;
import com.example.ems.repository.EmployeeRepository;
import com.example.ems.service.EmployeeService;

@RunWith(SpringRunner.class)
@SpringBootTest
class EmployeeManagementSystemApiApplicationTests {
	
	@Autowired
	private EmployeeService employeeService;

	@MockBean
	private EmployeeRepository employeeRepository;
	
	@Test
	public void getEmployeesTest() {
		when(employeeRepository.findAll()).thenReturn(Stream.of(new Employee("Raushan", "raushan@gmail.com", "339383"), new Employee("RK", "rk@gmail.com", "58393")).collect(Collectors.toList()));

		assertEquals(2, employeeService.getAllEmp().size());
	}
	
	@Test
	public void getEmployeeByIdTest() {
		Optional<Employee> employee = Optional.of(new Employee("Raushan", "raushan@gmail.com", "339383"));
		when(employeeRepository.findById(1L)).thenReturn(employee);
		Employee emp = employeeService.getByIdEmp(1L);
		assertEquals("Raushan", emp.getName());
		assertEquals("raushan@gmail.com", emp.getEmail());
		assertEquals("339383", emp.getMobileno());
	}
	
	@Test
	public void saveEmployeeTest() {
		Employee emp = new Employee("Raushan", "kumar@gmail.com", "49020");
		when(employeeRepository.save(emp)).thenReturn(emp);
		assertEquals(emp, employeeService.saveEmp(emp));
	}

	@Test
	public void deleteEmployeeTest() {
	
		this.employeeService.deleteEmp(1L);
		verify(this.employeeRepository, times(1)).deleteById(1L);
	}
}
