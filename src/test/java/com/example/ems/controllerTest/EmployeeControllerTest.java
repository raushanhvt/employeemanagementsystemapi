package com.example.ems.controllerTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import com.example.ems.controller.EmployeeController;
import com.example.ems.entity.Employee;
import com.example.ems.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@WebMvcTest(EmployeeController.class)
public class EmployeeControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper mapper;

	@MockBean
	private EmployeeService employeeService;



	@Test
	void saveEmployeeTest() throws Exception {
		Employee emp = new Employee("rk", "rk@gmail.com", "3383938");

		when(employeeService.saveEmp(emp)).thenReturn(emp);

		mockMvc.perform(
				post("/api/employees").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(emp)))
				.andExpect(status().isOk()).andExpect((ResultMatcher) jsonPath("$[0].name", emp.getName()));

//		Employee employee = employeeService.save(emp);
	//	assertEquals(controller.addEmployee(emp), emp);

	}

}
