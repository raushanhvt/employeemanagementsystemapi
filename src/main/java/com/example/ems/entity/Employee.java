package com.example.ems.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "employee")
@ApiModel
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(notes = "The database generated product ID")
	private Long id;

	@Column(name = "name")
	@ApiModelProperty(value = "Name of Emp")
	private String name;

	@Column(name = "email")
	@ApiModelProperty(value = "Email of Emp")
	private String email;

	@Column(name = "mobileno")
	@ApiModelProperty(value = "Mobileno of Emp")
	private String mobileno;

	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public Employee(String name, String email, String mobileno) {
		super();
		this.name = name;
		this.email = email;
		this.mobileno = mobileno;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

}
