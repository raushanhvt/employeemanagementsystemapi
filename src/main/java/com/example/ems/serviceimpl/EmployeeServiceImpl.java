package com.example.ems.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ems.entity.Employee;
import com.example.ems.repository.EmployeeRepository;
import com.example.ems.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	
	private EmployeeRepository employeeRepository;

	public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
		super();
		this.employeeRepository = employeeRepository;
	}

	@Override
	public List<Employee> getAllEmp() {
		return employeeRepository.findAll();
	}

	@Override
	public Employee getByIdEmp(Long id) {
		return employeeRepository.findById(id).get();
	}

	@Override
	public Employee saveEmp(Employee emp) {
		return employeeRepository.save(emp);
	}

	@Override
	public Employee updateEmp(Employee emp, Long id) {
		Employee xemp = employeeRepository.findById(id).get();
		xemp.setId(id);
		xemp.setName(emp.getName());
		xemp.setEmail(emp.getEmail());
		xemp.setMobileno(emp.getMobileno());
		
		return employeeRepository.save(xemp);
	}

	@Override
	public void deleteEmp(Long id) {
		
		employeeRepository.deleteById(id);
		
	}
	
	
	
	
}
